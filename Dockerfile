FROM alpine as builder

COPY qemu-* /usr/bin/
COPY configure.sh /usr/local/bin/configure
COPY daemon.sh /usr/bin/my-daemon
FROM builder

LABEL maintainer="Jay MOULIN <https://jaymoulin.me/me/docker-transmission> <https://twitter.com/MoulinJay>"
ARG VERSION=2.94
LABEL version=${VERSION}

RUN echo "http://dl-4.alpinelinux.org/alpine/edge/main/" >> /etc/apk/repositories && \
apk add transmission-daemon curl --no-cache && \
curl "https://raw.githubusercontent.com/ronggang/transmission-web-control/master/release/install-tr-control.sh" > /tmp/install.sh && \
(echo 1 | sh /tmp/install.sh) && \
rm /tmp/install.sh && \
mkdir /output && \
mkdir /to_download && \
mkdir /config &&\
chmod 777 /output && \
chmod 777 /to_download && \
chmod 777 /config && \
apk del curl --purge
WORKDIR /usr/bin

EXPOSE 9091
EXPOSE 51413
EXPOSE 51413/udp

VOLUME /output
VOLUME /to_download
VOLUME /config

ENV USERNAME=admin
ENV PASSWORD=admin
ENV PORT=9091

CMD my-daemon
